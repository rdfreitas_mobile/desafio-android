package br.com.concrete.github.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateUtils {

    private static final String DATE_BRAZILIAN = "yyyy-MM-dd'T'HH:mm:ss";
    private static final Locale BRAZIL = new Locale("pt","BR");

    public static String formatDate( String dateString) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_BRAZILIAN, BRAZIL);
        try {
            Date date = simpleDateFormat.parse(dateString);
            SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy", BRAZIL);
            return formatDate.format(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;

    }
}
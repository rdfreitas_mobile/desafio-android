package br.com.concrete.github.application.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Repository implements Parcelable {

    @SerializedName("name")
    private String name;

    @SerializedName("full_name")
    private String fullName;

    @SerializedName("description")
    private String descriptionName;

    @SerializedName("forks_count")
    private int numberForks;

    @SerializedName("stargazers_count")
    private int numberStars;

    @SerializedName("owner")
    private User user;

    public Repository(Parcel in) {
        this.name = in.readString();
        this.fullName = in.readString();
        this.descriptionName = in.readString();
        this.numberForks = in.readInt();
        this.numberStars = in.readInt();
        this.user = in.readParcelable(User.class.getClassLoader());
    }

    public String getFullName() {
        return fullName;
    }

    public User getUser() {
        return user;
    }

    public String getName() {
        return name;
    }

    public String getDescriptionName() {
        return descriptionName;
    }

    public int getNumberForks() {
        return numberForks;
    }

    public int getNumberStars() {
        return numberStars;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.fullName);
        dest.writeString(this.descriptionName);
        dest.writeInt(this.numberForks);
        dest.writeInt(this.numberStars);
        dest.writeParcelable(this.user, flags);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Repository createFromParcel(Parcel in) {
            return new Repository(in);
        }

        public Repository[] newArray(int size) {
            return new Repository[size];
        }
    };
}

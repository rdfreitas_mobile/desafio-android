package br.com.concrete.github.pulls;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.List;

import br.com.concrete.github.R;
import br.com.concrete.github.application.listener.OnItemClickListener;
import br.com.concrete.github.application.model.Pulls;
import br.com.concrete.github.application.model.Repository;
import br.com.concrete.github.application.view.BaseActivity;
import br.com.concrete.github.component.FontTextView;
import butterknife.BindView;

public class PullsActivity extends BaseActivity implements PullsView, OnItemClickListener {

    protected PullsPresenter pullsPresenter;
    protected Repository repository;
    protected PullsAdapter repositoryAdapter;

    @BindView(R.id.itemsRecyclerView)
    protected RecyclerView mRecyclerView;

    @BindView(R.id.pulls_closed)
    protected FontTextView pullsclosed;

    @BindView(R.id.pulls_opened)
    protected FontTextView pullsOpened;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pulls);

        extrasRepository();
        configToolbarBack();
        setNameTitleToolbar();
        initItems();
        initPresenter();
    }

    private void initItems() {
        repositoryAdapter = new PullsAdapter(this);
        repositoryAdapter.setOnClickListener(this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(mRecyclerView.getContext(),
                linearLayoutManager.getOrientation());
        mRecyclerView.addItemDecoration(mDividerItemDecoration);
    }

    private void extrasRepository() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            repository =  bundle.getParcelable(getString(R.string.repository_extra));
        }
    }

    private void configToolbarBack() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void setNameTitleToolbar() {
        fontTextViewTitleToolbar.setText(repository.getName());
    }

    private void initPresenter() {
        pullsPresenter = new PullsPresenter(this);
        pullsPresenter.getPulls(repository.getUser().getName(), repository.getName());
    }

    private void showTotalPulls(String countOpened, String countClosed) {
        pullsOpened.setText(countOpened);
        pullsclosed.setText(countClosed);
    }

    private void openWebUrl(String url) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    @Override
    public void requestSuccessful(List<Pulls> pulls, String countOpened, String countClosed) {
        repositoryAdapter.setPulls(pulls);
        mRecyclerView.setAdapter(repositoryAdapter);

        showTotalPulls(countOpened, countClosed);
    }

    @Override
    public void onItemClick(View view, Object object) {
        Pulls pull = (Pulls) object;
        openWebUrl(pull.getUrl());
    }
}

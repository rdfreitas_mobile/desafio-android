package br.com.concrete.github.application.view;

public interface BaseView {

    void showLoading();

    void hideLoading();

    void showMessage(String title, String message);
}
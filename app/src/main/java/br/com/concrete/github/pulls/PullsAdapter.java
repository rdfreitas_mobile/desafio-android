package br.com.concrete.github.pulls;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.concrete.github.R;
import br.com.concrete.github.application.listener.OnItemClickListener;
import br.com.concrete.github.application.model.Pulls;
import br.com.concrete.github.utils.CircleTransform;
import br.com.concrete.github.utils.DateUtils;
import butterknife.BindView;
import butterknife.ButterKnife;

class PullsAdapter extends RecyclerView.Adapter<PullsAdapter.PullsViewHolder> implements View.OnClickListener {

    private OnItemClickListener mListener;
    private List<Pulls> pulls;
    private Context context;

    PullsAdapter(Context context) {
        this.context = context;
    }

    public void setPulls(List<Pulls> pulls) {
        this.pulls = pulls;
    }

    @Override
    public PullsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.view_pulls_item, parent, false);
        return new PullsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PullsViewHolder holder, int position) {
        Pulls pull = pulls.get(position);
        setItemView(holder, pull);
    }

    private void setItemView(PullsViewHolder holder, Pulls pulls) {
        try {
            holder.itemView.setTag(pulls);
            holder.itemView.setOnClickListener(this);

            holder.title.setText(pulls.getTitle());
            holder.description.setText(pulls.getBody());

            holder.user.setText(pulls.getUser().getName());

            holder.name.setText(DateUtils.formatDate(pulls.getDateCreate()));

            Picasso.with(context).load(pulls.getUser().getPhoto()).
                    transform(new CircleTransform()).into(holder.imageProfile);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return pulls.size();
    }

    @Override
    public void onClick(View v) {
        if(mListener != null) {
            mListener.onItemClick(v, v.getTag());
        }
    }

    class PullsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.view_pulls_title)
        TextView title;

        @BindView(R.id.view_pulls_description)
        TextView description;

        @BindView(R.id.view_pulls_name)
        TextView name;

        @BindView(R.id.view_pulls_user)
        TextView user;

        @BindView(R.id.view_pulls_image)
        ImageView imageProfile;


        PullsViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    void setOnClickListener(OnItemClickListener mListener) {
        this.mListener = mListener;
    }
}

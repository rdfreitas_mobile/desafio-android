package br.com.concrete.github.repository;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.List;

import br.com.concrete.github.R;
import br.com.concrete.github.application.listener.EndlessRecyclerViewScrollListener;
import br.com.concrete.github.application.listener.OnItemClickListener;
import br.com.concrete.github.application.model.Repository;
import br.com.concrete.github.application.view.BaseActivity;
import br.com.concrete.github.pulls.PullsActivity;
import butterknife.BindView;

public class RepositoryActivity extends BaseActivity implements RepositoryView, OnItemClickListener {

    protected int page;
    protected RepositoryAdapter repositoryAdapter;
    protected RepositoryPresenter mainPresenter;

    protected EndlessRecyclerViewScrollListener scrollListener;

    @BindView(R.id.itemsRecyclerView)
    protected RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repository);

        initItems();
        initPresenter();
    }

    private void initItems() {
        page = 1;

        repositoryAdapter = new RepositoryAdapter(this);
        repositoryAdapter.setOnClickListener(this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(mRecyclerView.getContext(),
                linearLayoutManager.getOrientation());
        mRecyclerView.addItemDecoration(mDividerItemDecoration);

        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                RepositoryActivity.this.page = page + 1;
                mainPresenter.getRepositories(RepositoryActivity.this.page);
            }
        };

        mRecyclerView.addOnScrollListener(scrollListener);
    }

    private void initPresenter() {
        mainPresenter = new RepositoryPresenter(this);
        mainPresenter.getRepositories(page);
    }

    @Override
    public void requestSuccessful(List<Repository> repositories) {
        if(page > 1) {
            repositoryAdapter.updatePageRepositories(repositories);
        } else {
            repositoryAdapter.setRepositories(repositories);
            mRecyclerView.setAdapter(repositoryAdapter);
        }
    }

    @Override
    public void onItemClick(View view, Object object) {
        Repository repository = (Repository) object;
        Intent intent = new Intent(this, PullsActivity.class);
        intent.putExtra(getString(R.string.repository_extra), repository);
        startActivity(intent);
    }
}

package br.com.concrete.github.repository;

import java.util.List;

import br.com.concrete.github.R;
import br.com.concrete.github.application.model.Repository;
import br.com.concrete.github.application.model.ResponseRoot;
import br.com.concrete.github.application.presenter.BasePresenter;
import br.com.concrete.github.application.service.GitHubService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class RepositoryPresenter extends BasePresenter<RepositoryView> {

    RepositoryPresenter(RepositoryView view) {
        super(view);
    }

    void getRepositories(int page) {

        mView.showLoading();

        Call<ResponseRoot> call = GitHubService.apiService(mContext).getRepositories(mContext.getString(R.string.query_repository),
                mContext.getString(R.string.query_repository), page);
        call.enqueue(new Callback<ResponseRoot>() {
            @Override
            public void onResponse(Call<ResponseRoot> call, Response<ResponseRoot> response) {
                List<Repository> repositories = response.body().getRepositories();
                mView.requestSuccessful(repositories);
                mView.hideLoading();
            }

            @Override
            public void onFailure(Call<ResponseRoot> call, Throwable t) {
                mView.hideLoading();
                mView.showMessage(mContext.getString(R.string.error_dialog_title),
                        mContext.getString(R.string.error_message_dialog));
            }
        });
    }
}

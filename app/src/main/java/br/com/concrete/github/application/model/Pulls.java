package br.com.concrete.github.application.model;

import com.google.gson.annotations.SerializedName;

public class Pulls {

    @SerializedName("html_url")
    private String url;

    @SerializedName("title")
    private String title;

    @SerializedName("body")
    private String body;

    @SerializedName("created_at")
    private String dateCreate;

    @SerializedName("closed_at")
    private String close;

    @SerializedName("user")
    private User user;

    public String getClose() {
        return close;
    }

    public String getUrl() {
        return url;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    public String getDateCreate() {
        return dateCreate;
    }

    public User getUser() {
        return user;
    }
}

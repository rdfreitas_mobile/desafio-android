package br.com.concrete.github.repository;

import java.util.List;

import br.com.concrete.github.application.model.Repository;
import br.com.concrete.github.application.view.BaseView;

interface RepositoryView extends BaseView {

    void requestSuccessful(List<Repository> repositories);

}

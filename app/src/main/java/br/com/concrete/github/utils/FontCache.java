package br.com.concrete.github.utils;

import android.content.Context;
import android.graphics.Typeface;

import java.util.Hashtable;

public class FontCache {

    private static Hashtable<String, Typeface> fontCache = new Hashtable<>();

    private FontCache() {
    }

    @SuppressWarnings("checkstyle:illegalCatch")
    public static Typeface getTypeface(String name, Context context) {
        Typeface tf = fontCache.get(name);

        if (tf == null) {
            try {
                tf = Typeface.createFromAsset(context.getAssets(), name);
            } catch (RuntimeException e) {
                return null;
            }

            fontCache.put(name, tf);
        }
        return tf;
    }
}

package br.com.concrete.github.repository;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.concrete.github.R;
import br.com.concrete.github.application.listener.OnItemClickListener;
import br.com.concrete.github.application.model.Repository;
import br.com.concrete.github.utils.CircleTransform;
import butterknife.BindView;
import butterknife.ButterKnife;

class RepositoryAdapter extends RecyclerView.Adapter<RepositoryAdapter.RepositoryViewHolder> implements View.OnClickListener {

    private OnItemClickListener mListener;
    private List<Repository> repositories;
    private Context context;

    RepositoryAdapter(Context context) {
        this.context = context;
    }

    void setRepositories(List<Repository> repositories){
        this.repositories = repositories;
    }

    void updatePageRepositories(List<Repository> repositories){
        this.repositories.addAll(repositories);
        notifyDataSetChanged();
    }

    @Override
    public RepositoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.view_repository_item, parent, false);
        return new RepositoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RepositoryViewHolder holder, int position) {
        Repository repository = repositories.get(position);
        setItemView(holder, repository);
    }

    @Override
    public int getItemCount() {
        return repositories.size();
    }

    private void setItemView(RepositoryViewHolder holder, Repository repository) {
        try {
            holder.itemView.setTag(repository);
            holder.itemView.setOnClickListener(this);

            holder.title.setText(repository.getName());
            holder.description.setText(repository.getDescriptionName());
            holder.starts.setText(String.valueOf(repository.getNumberStars()));
            holder.forks.setText(String.valueOf(repository.getNumberForks()));

            holder.user.setText(repository.getUser().getName());
            holder.name.setText(repository.getFullName());

            Picasso.with(context).load(repository.getUser().getPhoto()).
                    transform(new CircleTransform()).into(holder.imageProfile);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        if(mListener != null) {
            mListener.onItemClick(v, v.getTag());
        }
    }

    class RepositoryViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.view_repository_title)
        TextView title;

        @BindView(R.id.view_repository_description)
        TextView description;

        @BindView(R.id.view_repository_forks_count)
        TextView forks;

        @BindView(R.id.view_repository_start_count)
        TextView starts;

        @BindView(R.id.view_repository_image_profile)
        ImageView imageProfile;

        @BindView(R.id.view_repository_user)
        TextView user;

        @BindView(R.id.view_repository_name)
        TextView name;

        RepositoryViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    void setOnClickListener(OnItemClickListener mListener) {
        this.mListener = mListener;
    }
}





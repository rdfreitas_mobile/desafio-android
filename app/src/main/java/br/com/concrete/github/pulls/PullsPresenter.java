package br.com.concrete.github.pulls;

import java.util.List;

import br.com.concrete.github.R;
import br.com.concrete.github.application.model.Pulls;
import br.com.concrete.github.application.presenter.BasePresenter;
import br.com.concrete.github.application.service.GitHubService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class PullsPresenter extends BasePresenter<PullsView> {

    PullsPresenter(PullsView view) {
        super(view);
    }

    void getPulls(String owner, String name) {

        mView.showLoading();

        Call<List<Pulls>> call = GitHubService.apiService(mContext).getPulls(owner, name);
        call.enqueue(new Callback<List<Pulls>>() {
            @Override
            public void onResponse(Call<List<Pulls>> call, Response<List<Pulls>> response) {
                List<Pulls> pulls = response.body();

                int countOpened = 0;
                int countClosed = 0;

                for (Pulls pull: pulls) {
                    if (pull.getClose() == null) {
                        countOpened += 1;
                    } else {
                        countClosed += 1;
                    }
                }

                String resultOpened = String.valueOf(countOpened).concat(" ").concat(mContext.getString(R.string.opened));
                String resultClosed  = " / ".concat(String.valueOf(countClosed).concat(" ").concat(mContext.getString(R.string.closed)));

                mView.requestSuccessful(pulls, resultOpened, resultClosed);
                mView.hideLoading();
            }

            @Override
            public void onFailure(Call<List<Pulls>> call, Throwable t) {
                mView.hideLoading();
                mView.showMessage(mContext.getString(R.string.error_dialog_title),
                        mContext.getString(R.string.error_message_dialog));
            }
        });
    }
}

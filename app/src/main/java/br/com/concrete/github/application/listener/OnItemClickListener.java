package br.com.concrete.github.application.listener;

import android.view.View;

public interface OnItemClickListener {
    void onItemClick(View view, Object object);
}

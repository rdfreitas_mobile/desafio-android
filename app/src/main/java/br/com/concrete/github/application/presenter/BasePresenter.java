package br.com.concrete.github.application.presenter;

import android.content.Context;

import br.com.concrete.github.application.view.BaseView;

public class BasePresenter<T extends BaseView> {

    private static final String TAG = BasePresenter.class.getName();

    protected Context mContext;
    protected T mView;

    protected BasePresenter(T view) {

        mContext = (Context) view;
        mView = view;
    }

    protected BasePresenter(Context context, T view) {
        mContext = context;
        mView = view;
    }
}

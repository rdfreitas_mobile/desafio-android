package br.com.concrete.github.pulls;

import java.util.List;

import br.com.concrete.github.application.model.Pulls;
import br.com.concrete.github.application.view.BaseView;

interface PullsView extends BaseView{
    void requestSuccessful(List<Pulls> repositoriesm , String countOpened, String countClosed);
}
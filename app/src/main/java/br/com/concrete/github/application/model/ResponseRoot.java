package br.com.concrete.github.application.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseRoot {

    @SerializedName("total_count")
    private int totalCount;

    @SerializedName("incomplete_results")
    private boolean incompleteResuts;

    @SerializedName("items")
    private List<Repository> repositories;

    public int getTotalCount() {
        return totalCount;
    }

    public boolean isIncompleteResuts() {
        return incompleteResuts;
    }

    public List<Repository> getRepositories() {
        return repositories;
    }
}

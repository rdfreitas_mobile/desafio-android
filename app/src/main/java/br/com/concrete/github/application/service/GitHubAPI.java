package br.com.concrete.github.application.service;

import java.util.List;

import br.com.concrete.github.application.model.Pulls;
import br.com.concrete.github.application.model.ResponseRoot;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface GitHubAPI {

    @GET("search/repositories?")
    Call<ResponseRoot> getRepositories(@Query("q") String q, @Query("sort") String sorting, @Query("page") int numPage);

    @GET("repos/{owner}/{name}/pulls")
    Call<List<Pulls>> getPulls(@Path("owner") String owner, @Path("name") String name);
}

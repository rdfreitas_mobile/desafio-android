package br.com.concrete.github.application.service;

import android.annotation.SuppressLint;
import android.content.Context;

import java.io.File;
import java.io.IOException;

import br.com.concrete.github.BuildConfig;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class GitHubService {

    private static Retrofit retrofit = null;

    private static Retrofit getClient(Context context) {
        if (retrofit == null) {

            File httpCacheDirectory = new File(context.getCacheDir(), "responses");
            int cacheSize = 10*1024*1024;
            Cache cache = new Cache(httpCacheDirectory, cacheSize);

            OkHttpClient client = new OkHttpClient.Builder()
                    .addNetworkInterceptor(REWRITE_CACHE_CONTROL_INTERCEPTOR)
                    .cache(cache)
                    .build();

            retrofit = new Retrofit.Builder().baseUrl(BuildConfig.URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create()).build();
        }
        return retrofit;
    }

    public static GitHubAPI apiService(Context context){
        return getClient(context).create(GitHubAPI.class);
    }

    private static final Interceptor REWRITE_CACHE_CONTROL_INTERCEPTOR = new Interceptor() {
        @SuppressLint("DefaultLocale")
        @Override
        public Response intercept(Chain chain) throws IOException {
            Response originalResponse = chain.proceed(chain.request());
            return originalResponse.newBuilder()
                    .header("Cache-Control", String.format("max-age=%d, only-if-cached, max-stale=%d", 120, 0))
                    .build();
        }
    };
}


